import {StyleProp, TextInputProps, ViewStyle} from 'react-native';

export type TPasswordInputProps = {
  label?: string;
  wrapperStyle?: StyleProp<ViewStyle>;
  errorMessage?: string;
} & TextInputProps;
