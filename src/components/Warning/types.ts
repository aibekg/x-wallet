export type TWarningProps = {
  title?: string;
  text?: string;
};
