import React, {FC} from 'react';
import {TouchableWithoutFeedback, Keyboard} from 'react-native';

import {TDismissKeyboardViewProps} from './types';

const DismissKeyboardView: FC<TDismissKeyboardViewProps> = ({children}) => {
  const handlePress = () => {
    Keyboard.dismiss();
  };
  return (
    <TouchableWithoutFeedback onPress={handlePress}>
      {children}
    </TouchableWithoutFeedback>
  );
};

export default DismissKeyboardView;
