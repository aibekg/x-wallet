export const cutStr = (str: string) =>
  str.length > 13 ? `${str.slice(0, 5)}...${str.slice(-5)}` : str;

export function isPrivateKey(sig: string) {
  if (!sig) {
    return false;
  }
  if (sig.length === 64) {
    return true;
  }
}

export const getSecretList = (seeds: string) => {
  const list = seeds.split(' ');
  return [
    `${list.shift()} *** ***`,
    '*** *** ***',
    '*** *** ***',
    `*** *** ${list.pop()}`,
  ];
};

export function truncate(value: string, length: number) {
  if (value?.length <= length) {
    return value;
  }

  const separator = '...';
  const stringLength = length - separator.length;
  const frontLength = Math.ceil(stringLength / 2);
  const backLength = Math.floor(stringLength / 2);

  return (
    value.substring(0, frontLength) +
    separator +
    value.substring(value.length - backLength)
  );
}
