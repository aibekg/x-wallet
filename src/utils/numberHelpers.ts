import BigNumber from 'bignumber.js';

export const bigNumberConverter = (value: any, decimalPlaces = 2) => {
  return BigNumber(value).decimalPlaces(decimalPlaces).toNumber();
};

export const reduceBalance = (balance: any, precision = 6) => {
  if (balance) {
    if (balance.int) {
      balance = balance.int;
    }
    if (balance.decimal) {
      balance = balance.decimal;
    }
    if (parseFloat(balance) % 1 === 0) {
      return parseInt(balance);
    }
    return (
      Math.trunc(parseFloat(balance) * Math.pow(10, precision)) /
      Math.pow(10, precision)
    );
  }
  if (balance === 0) {
    return 0;
  }
  return 0;
};

export const getTokenUsdPriceByLiquidity = (
  liquidity0: number,
  liquidity1: number,
  usdPrice: number,
  precision?: number,
) => {
  const liquidityRatio = BigNumber(liquidity0, 10)
    .dividedBy(BigNumber(liquidity1, 10))
    .toNumber();
  return bigNumberConverter(liquidityRatio * usdPrice, precision || 8);
};
