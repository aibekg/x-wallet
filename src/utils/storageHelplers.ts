import {useCallback, useEffect, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

export function useAsyncStorage(key: string, initialValue?: any) {
  const [storedValue, setStoredValue] = useState<any>(initialValue || null);

  const getStoredItem = useCallback(
    async (keyParam: string, initialValueParam?: any) => {
      try {
        const item = await AsyncStorage.getItem(keyParam);
        const value = item ? JSON.parse(item) : initialValueParam || null;
        setStoredValue(value);
      } catch (error) {}
    },
    [],
  );

  useEffect(() => {
    getStoredItem(key, initialValue);
  }, [key, initialValue]);

  const setValue = useCallback(
    async (value: any) => {
      try {
        const valueToStore =
          value instanceof Function ? value(storedValue) : value;
        setStoredValue(valueToStore);
        await AsyncStorage.setItem(key, JSON.stringify(valueToStore));
      } catch (error) {}
    },
    [key],
  );

  return [storedValue, setValue];
}

export const getSavedValue = async (
  keyParam: string,
  initialValueParam?: any,
) => {
  try {
    const item = await AsyncStorage.getItem(keyParam);
    return item ? JSON.parse(item) : initialValueParam || null;
  } catch (error) {
    return initialValueParam || null;
  }
};

export const saveValue = async (key: string, value?: any) => {
  try {
    await AsyncStorage.setItem(key, JSON.stringify(value));
  } catch (error) {}
};
