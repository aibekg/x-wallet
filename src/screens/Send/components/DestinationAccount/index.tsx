import React, {FC, useCallback, useState} from 'react';
import {View, Text, TextInput, TouchableOpacity} from 'react-native';

import {styles} from './styles';
import {TDestinationAccountProps} from './types';
import WalletItem from '../../../../components/WalletItem';
import CircleXSvg from '../../../../assets/images/circle-x.svg';

const DestinationAccount: FC<TDestinationAccountProps> = React.memo(
  ({selectedAccount, setSelectedAccount}) => {
    const [name, setName] = useState<string>('');

    const handlePressReturn = useCallback(() => {
      if (name) {
        setSelectedAccount && setSelectedAccount(name);
        setName('');
      }
    }, [name, setSelectedAccount]);

    const handlePressErase = useCallback(() => {
      setSelectedAccount && setSelectedAccount('');
    }, [setSelectedAccount]);

    return (
      <View style={styles.wrapper}>
        <Text style={styles.label}>Destination Account</Text>
        {selectedAccount ? (
          <View style={styles.selectedAccountWrapper}>
            <WalletItem
              name={selectedAccount}
              textStyle={styles.selectedAccountText}
            />
            <TouchableOpacity activeOpacity={0.8} onPress={handlePressErase}>
              <CircleXSvg />
            </TouchableOpacity>
          </View>
        ) : (
          <TextInput
            style={styles.input}
            placeholder="Enter destination account"
            autoCapitalize="none"
            autoFocus={true}
            onEndEditing={handlePressReturn}
            onBlur={handlePressReturn}
            onSubmitEditing={handlePressReturn}
            onChangeText={setName}
            value={name}
          />
        )}
      </View>
    );
  },
);

export default DestinationAccount;
