import {StyleSheet} from 'react-native';
import {bottomSpace} from '../../utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  topHeaderContent: {
    marginTop: 12,
    marginBottom: 24,
    paddingHorizontal: 20,
    width: '100%',
  },
  contentWrapper: {
    width: '100%',
    flex: 1,
  },
  content: {
    width: '100%',
  },
  footer: {
    marginBottom: bottomSpace + 24,
    width: '100%',
  },
  margin: {
    width: '100%',
    height: 20,
    marginBottom: 24,
    borderBottomColor: 'rgba(223,223,237,0.5)',
    borderBottomWidth: 1,
  },
  chainWrapper: {
    zIndex: 9,
  },
  chainSecondWrapper: {
    zIndex: 8,
  },
});
