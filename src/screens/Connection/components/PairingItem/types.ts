export type TPairingItem = {
  logo?: string;
  name?: string;
  url?: string;
  topic?: string;
};

export type TPairingItemProps = {
  item: TPairingItem;
  onDelete?: () => void;
};
