import React, {FC, useCallback} from 'react';
import {Text, View, TouchableOpacity, Image, Linking} from 'react-native';
import {TPairingItemProps} from './types';
import TrashEmptySvg from '../../../../assets/images/trash-empty.svg';
import {styles} from './styles';
import {truncate} from '../../../../utils/stringHelpers';

const PairingItem: FC<TPairingItemProps> = React.memo(
  ({item: pairingItem, onDelete}) => {
    const {logo, name, url} = pairingItem;

    const onLinkPress = useCallback(() => {
      if (url) {
        Linking.openURL(url);
      }
    }, [url]);

    return (
      <View style={styles.wrapper}>
        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.container}
          disabled={!url}
          onPress={onLinkPress}>
          <View style={styles.rightSide}>
            {logo ? (
              <View style={styles.iconWrapper}>
                <Image
                  source={{uri: logo}}
                  style={styles.logo}
                  resizeMode="contain"
                />
              </View>
            ) : null}
            <View style={styles.center}>
              <Text style={styles.title}>{name}</Text>
              {url ? (
                <Text style={styles.link}>
                  {truncate(url?.split('https://')[1] ?? 'Unknown', 23)}
                </Text>
              ) : null}
            </View>
          </View>
          <TouchableOpacity
            hitSlop={{
              bottom: 12,
              top: 12,
              right: 12,
              left: 12,
            }}
            activeOpacity={0.8}
            onPress={onDelete}
            style={styles.delete}>
            <TrashEmptySvg width={24} height={24} />
          </TouchableOpacity>
        </TouchableOpacity>
      </View>
    );
  },
);

export default PairingItem;
