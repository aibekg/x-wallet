import React, {useCallback, useRef} from 'react';
import {
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  Platform,
  Alert,
} from 'react-native';
import {useDispatch} from 'react-redux';
import {useForm, Controller, FieldValues} from 'react-hook-form';
import KeyboardSpacer from 'react-native-keyboard-spacer';

import Logo from '../../assets/images/logo.svg';
import ArrowLeftSvg from '../../assets/images/arrow-left.svg';

import {styles} from './styles';
import {ERootStackRoutes, TNavigationProp} from '../../routes/types';
import {setPassword, setPhrases} from '../../store/auth';
import PasswordInput from '../../components/PasswordInput';
import {useScrollBottomOnKeyboard} from '../../utils/keyboardHelpers';
import {recoverySchema} from '../../validation/recoverySchema';
import queryString from 'query-string';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import api from '../../api';
import {getRestoreAccount} from '../../store/userWallet/actions';
import {useNavigation} from '@react-navigation/native';
import {bottomSpace} from '../../utils/deviceHelpers';

const bgImage = require('../../assets/images/bgimage.png');

const RecoveryFromSeeds = () => {
  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.RecoveryFromSeeds>>();

  const dispatch = useDispatch();

  const {
    control,
    handleSubmit,
    formState: {errors, isValid},
  } = useForm({
    resolver: recoverySchema,
    mode: 'onChange',
  });

  const handlePressBack = useCallback(() => {
    navigation.goBack();
  }, [navigation]);

  const handlePressRecover = useCallback(
    (data: FieldValues) => {
      api
        .get(
          `/api/validate-seeds?${queryString.stringify({
            seeds: data.seeds || '',
          })}`,
        )
        .then(response => {
          if (response.data) {
            api
              .get(
                `/api/hash-password?${queryString.stringify({
                  password: data.password || '',
                })}`,
              )
              .then(hashResponse => {
                if (hashResponse.data.hash) {
                  dispatch(setPassword(hashResponse.data.hash));
                  dispatch(setPhrases(data.seeds.split(' ')));
                  dispatch(
                    getRestoreAccount({
                      seeds: data.seeds || '',
                      accountIndex: 0,
                    }),
                  );
                  navigation.navigate({
                    name: ERootStackRoutes.SignIn,
                    params: undefined,
                  });
                } else {
                  ReactNativeHapticFeedback.trigger('impactMedium', {
                    enableVibrateFallback: false,
                    ignoreAndroidSystemSettings: false,
                  });
                  Alert.alert(
                    'Failed to import the account',
                    'Something went wrong. Please try again later.',
                  );
                }
              })
              .catch(() => {
                ReactNativeHapticFeedback.trigger('impactMedium', {
                  enableVibrateFallback: false,
                  ignoreAndroidSystemSettings: false,
                });
                Alert.alert(
                  'Failed to import the account',
                  'Something went wrong. Please try again later.',
                );
              });
          } else {
            ReactNativeHapticFeedback.trigger('impactMedium', {
              enableVibrateFallback: false,
              ignoreAndroidSystemSettings: false,
            });
            Alert.alert(
              'Failed to import the account',
              'Invalid secret recovery phrase or deleted account',
            );
          }
        })
        .catch(() => {
          ReactNativeHapticFeedback.trigger('impactMedium', {
            enableVibrateFallback: false,
            ignoreAndroidSystemSettings: false,
          });
          Alert.alert(
            'Failed to import the account',
            'Invalid secret recovery phrase or deleted account',
          );
        });
    },
    [navigation],
  );

  const scrollRef = useRef<ScrollView | null>(null);
  useScrollBottomOnKeyboard(scrollRef);

  return (
    <ImageBackground source={bgImage} resizeMode="cover" style={styles.bgImage}>
      <ScrollView
        ref={scrollRef}
        keyboardShouldPersistTaps="handled"
        showsVerticalScrollIndicator={false}
        style={styles.contentWrapper}
        contentContainerStyle={styles.content}>
        <Logo width={50} height={50} />
        <Text style={styles.text}>
          {'Import a wallet with\nSecret Recovery Phrase'}
        </Text>
        <Controller
          control={control}
          name="seeds"
          render={({field: {onChange, onBlur, value}}) => (
            <PasswordInput
              wrapperStyle={styles.seeds}
              autoFocus={true}
              label="Secret Phrases (with spaces)"
              onChangeText={onChange}
              value={value}
              onBlur={onBlur}
              placeholder="Enter Secret Phrases"
              blurOnSubmit={true}
              errorMessage={errors.seeds?.message as string}
            />
          )}
        />
        <Controller
          control={control}
          name="password"
          render={({field: {onChange, onBlur, value}}) => (
            <PasswordInput
              wrapperStyle={styles.password}
              label="New Password"
              onChangeText={onChange}
              value={value}
              onBlur={onBlur}
              blurOnSubmit={true}
              errorMessage={errors.password?.message as string}
            />
          )}
        />
        <Controller
          control={control}
          name="confirmPassword"
          render={({field: {onChange, onBlur, value}}) => (
            <PasswordInput
              wrapperStyle={styles.confirmPassword}
              label="Confirm Password"
              onChangeText={onChange}
              value={value}
              onBlur={onBlur}
              blurOnSubmit={true}
              errorMessage={errors.confirmPassword?.message as string}
              onSubmitEditing={handleSubmit(handlePressRecover)}
            />
          )}
        />
        <TouchableOpacity
          activeOpacity={0.8}
          disabled={!isValid}
          style={[styles.button, !isValid && styles.disabledBtn]}
          onPress={handleSubmit(handlePressRecover)}>
          <Text style={styles.buttonText}>Restore</Text>
        </TouchableOpacity>
        {Platform.OS === 'ios' && <KeyboardSpacer topSpacing={-bottomSpace} />}
      </ScrollView>
      <View style={styles.header}>
        <TouchableOpacity activeOpacity={0.8} onPress={handlePressBack}>
          <ArrowLeftSvg fill="white" />
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

export default RecoveryFromSeeds;
