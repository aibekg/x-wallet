import React, {useCallback, useState} from 'react';
import {Text, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import ArrowTopRightSvg from '../../../../assets/images/arrow-top-right.svg';
import ArrowBottomRightSvg from '../../../../assets/images/arrow-bottom-right.svg';
import Button from './components/Button';
import {styles} from './styles';
import {MAIN_COLOR} from '../../../../constants/styles';
import {ERootStackRoutes, TNavigationProp} from '../../../../routes/types';
import {
  makeSelectAccountBalanceUsd,
  makeSelectKDAWallet,
  makeSelectTotalBalanceUsd,
} from '../../../../store/userWallet/selectors';
import ReceiveKDAModal from '../../../../modals/ReceiveKDAModal';
import {useNavigation} from '@react-navigation/native';
import {setSelectedToken} from '../../../../store/userWallet';
import {useShallowEqualSelector} from '../../../../store/utils';

const WalletBalance = React.memo(() => {
  const navigation = useNavigation<TNavigationProp<ERootStackRoutes.Home>>();

  const dispatch = useDispatch();

  const kdaWallet = useShallowEqualSelector(makeSelectKDAWallet);
  const balanceUsd = useSelector(makeSelectAccountBalanceUsd);
  const balanceUsdTotal = useSelector(makeSelectTotalBalanceUsd);

  const [isKdaModalVisible, setKdaModalVisible] = useState(false);

  const handlePressSend = useCallback(() => {
    dispatch(setSelectedToken(kdaWallet));
    setTimeout(
      () =>
        navigation?.navigate({
          name: ERootStackRoutes.Send,
          params: {
            sourceChainId: '0',
          },
        }),
      150,
    );
  }, [kdaWallet, navigation]);

  const handlePressReceive = useCallback(() => {
    setKdaModalVisible(true);
  }, []);

  const closeKdaModal = useCallback(() => {
    setKdaModalVisible(false);
  }, []);

  return (
    <>
      <View style={styles.wrapper}>
        <View style={styles.netWorthContainer}>
          <Text style={styles.netWorthHeader}>{'Net Worth'}</Text>
          <Text style={styles.netWorth}>{`$ ${
            balanceUsdTotal > 0
              ? (Number(balanceUsdTotal) || 0).toFixed(2)
              : balanceUsdTotal
          }`}</Text>
        </View>
        <Text style={styles.balanceHeader}>{'Account Balance'}</Text>
        <Text style={styles.balance}>{`$ ${
          balanceUsd > 0 ? (Number(balanceUsd) || 0).toFixed(2) : balanceUsd
        }`}</Text>
        <View style={styles.buttonsWrapper}>
          <Button
            icon={<ArrowTopRightSvg fill="#FFA900" />}
            title="Send"
            onPress={handlePressSend}
            style={styles.button}
          />
          <Button
            title="Receive"
            style={styles.button}
            icon={<ArrowBottomRightSvg fill="#FFA900" />}
            backgroundColor="rgba(236,236,245,0.5)"
            textColor={MAIN_COLOR}
            onPress={handlePressReceive}
          />
        </View>
      </View>
      <ReceiveKDAModal isVisible={isKdaModalVisible} close={closeKdaModal} />
    </>
  );
});

export default WalletBalance;
