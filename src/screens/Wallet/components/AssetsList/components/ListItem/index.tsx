import React, {FC, useCallback, useMemo, useState} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';

import {styles} from './styles';
import {TListItemProps} from './types';
import TokenModal from '../../../../../../modals/TokenModal';
import {useDispatch} from 'react-redux';
import {setSelectedToken} from '../../../../../../store/userWallet';
import {makeSelectUsdEquivalents} from '../../../../../../store/userWallet/selectors';

import KDAIcon from '../../../../../../assets/images/kda-icon.svg';
import KDXIcon from '../../../../../../assets/images/kdx-icon.svg';
import KDLIcon from '../../../../../../assets/images/kdl-icon.svg';
import BabenaIcon from '../../../../../../assets/images/babena-icon.svg';
import HypeIcon from '../../../../../../assets/images/hype-icon.svg';
import WalletIcon from '../../../../../../assets/images/logo.svg';
import {useShallowEqualSelector} from '../../../../../../store/utils';

const ListItem: FC<TListItemProps> = React.memo(({walletItem, isFirst}) => {
  const dispatch = useDispatch();

  const [modalVisible, setModalVisible] = useState(false);

  const toggleModal = useCallback(() => {
    if (!modalVisible) {
      dispatch(setSelectedToken(walletItem));
    }
    setTimeout(() => setModalVisible(!modalVisible), 150);
  }, [modalVisible, walletItem]);

  const usdEquivalents = useShallowEqualSelector(makeSelectUsdEquivalents);

  const amount = useMemo(() => {
    const amountValue = Number(walletItem.totalAmount || 0);
    if (isNaN(amountValue)) {
      return 0;
    }
    return amountValue;
  }, [walletItem]);

  const currency = useMemo(() => {
    if (Array.isArray(usdEquivalents)) {
      const foundUsdValue = (usdEquivalents || []).find(
        item => item.token === walletItem.tokenAddress,
      );
      const usdAmount = amount * (foundUsdValue?.usd || 0);
      if (usdAmount) {
        return usdAmount.toFixed(2);
      }
    }
    return '';
  }, [walletItem, amount, usdEquivalents]);

  const canDelete = useMemo(
    () =>
      walletItem.tokenAddress !== 'coin' &&
      walletItem.tokenAddress !== 'kaddex.kdx',
    [walletItem],
  );

  const assetImageView = useMemo(() => {
    switch (walletItem?.tokenAddress) {
      case 'coin':
        return (
          <View style={styles.image}>
            <KDAIcon height="40" width="40" />
          </View>
        );
      case 'kaddex.kdx':
        return (
          <View style={styles.image}>
            <KDXIcon height="40" width="40" />
          </View>
        );
      case 'kdlaunch.token':
        return (
          <View style={styles.image}>
            <KDLIcon height="40" width="40" />
          </View>
        );
      case 'free.babena':
        return (
          <View style={styles.image}>
            <View style={styles.defaultImageBackground}>
              <BabenaIcon height="24" width="24" />
            </View>
          </View>
        );
      case 'hypercent.prod-hype-coin':
        return (
          <View style={styles.image}>
            <HypeIcon height="40" width="40" />
          </View>
        );
      case 'runonflux.flux':
        return (
          <Image
            source={require('../../../../../../assets/images/flux-icon.png')}
            style={styles.image}
          />
        );
      case 'kaddex.skdx':
        return (
          <Image
            source={require('../../../../../../assets/images/sdkx-icon.png')}
            style={styles.image}
          />
        );
      case 'lago.kwUSDC':
        return (
          <Image
            source={require('../../../../../../assets/images/usdc-icon.png')}
            style={styles.image}
          />
        );
      case 'free.anedak':
        return (
          <View style={styles.image}>
            <View style={styles.defaultImageBackground}>
              <Image
                source={require('../../../../../../assets/images/anedak-icon.png')}
                style={styles.imageContent}
              />
            </View>
          </View>
        );
      case 'lago.USD2':
        return (
          <Image
            source={require('../../../../../../assets/images/lago-icon.png')}
            style={styles.image}
          />
        );
      default:
        return (
          <View style={styles.image}>
            <View style={styles.defaultImageBackground}>
              <WalletIcon height="24" width="24" />
            </View>
          </View>
        );
    }
  }, [walletItem]);

  return (
    <>
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={toggleModal}
        disabled={amount <= 0}
        style={[styles.wrapper, isFirst && {borderTopWidth: 0}]}>
        <View style={styles.leftSide}>
          {assetImageView}
          <Text style={styles.title}>{`${amount.toFixed(6)} ${
            walletItem.tokenName
          }`}</Text>
        </View>
        <Text style={styles.currency}>{currency ? `$ ${currency}` : ''}</Text>
      </TouchableOpacity>
      <TokenModal
        canDelete={canDelete}
        toggle={toggleModal}
        isVisible={modalVisible}
      />
    </>
  );
});

export default ListItem;
