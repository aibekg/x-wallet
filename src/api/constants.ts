export const DEV_NETWORK_API_URL = 'https://devnet.kaddex.com';
export const TEST_NETWORK_API_URL = 'https://api.testnet.chainweb.com';
export const MAIN_NETWORK_API_URL = 'https://chainweb.kaddex.com';

export const SERVER_REMOTE_URL = 'https://proxy.kadena.architech.nyc';

export const KADDEX_NAMESPACE = 'kaddex';

export const defaultHeader = {
  Accept: '*/*',
};

export const blockJsonHeader = {
  Accept: 'application/json;blockheader-encoding=object',
};

export const jsonHeader = {
  Accept: 'application/json',
};

export const contentHeader = {
  'Content-Type': 'application/json',
};
