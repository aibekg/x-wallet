import {createSlice} from '@reduxjs/toolkit';

import {EGAsSpeed, TTransferState} from './types';
import {GAS_LIMIT, GAS_PRICE} from '../../constants';

const initialState: TTransferState = {
  gatheredTransferInfo: {amount: 0},
  transferResult: null,
  showTransferBubble: false,
  estimatedGasFee: {
    gasLimit: GAS_LIMIT,
    gasPrice: GAS_PRICE,
    speed: EGAsSpeed.NORMAL,
  },
};

const transfer = createSlice({
  name: 'transfer',
  initialState,
  reducers: {
    setGatheredTransferInfo: (state, {payload}) => {
      state.gatheredTransferInfo = {...state.gatheredTransferInfo, ...payload};
    },

    setTransferResult: (state, {payload}) => {
      state.transferResult = payload;
    },

    setTransferBubble: (state, {payload}) => {
      state.showTransferBubble = payload;
    },

    setEstimatedGasFee: (state, {payload}) => {
      state.estimatedGasFee = payload;
    },

    setInitialTransferState: state => {
      state.estimatedGasFee = initialState.estimatedGasFee;
      state.gatheredTransferInfo = initialState.gatheredTransferInfo;
      state.transferResult = initialState.transferResult;
    },
  },
});

export const {
  setTransferBubble,
  setGatheredTransferInfo,
  setTransferResult,
  setEstimatedGasFee,
  setInitialTransferState,
} = transfer.actions;

export default transfer.reducer;
