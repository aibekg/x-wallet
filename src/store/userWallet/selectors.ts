import {createSelector} from '@reduxjs/toolkit';

import {RootState} from '../store';
import {TAccount, TWallet} from './types';

const selectedState = (state: RootState) => state.userWallet;

export const makeSelectWalletInitialized = createSelector(
  selectedState,
  state => state.initialized,
);

export const makeSelectAccounts = createSelector(
  selectedState,
  state => state.accounts,
);

export const makeSelectSelectedAccount = createSelector(
  selectedState,
  state => state.selectedAccount,
);

export const makeSelectSearchTokenList = createSelector(
  selectedState,
  state => state.searchTokenList,
);

export const makeSelectIsConnectedWalletConnect = createSelector(
  selectedState,
  state => state.isConnectedWalletConnect,
);

export const makeSelectSelectedToken = createSelector(
  selectedState,
  state => state.selectedToken,
);

export const makeSelectBalanceLoading = createSelector(
  selectedState,
  state => state.balanceDetailState.fetching,
);

export const makeSelectWallets = createSelector(
  selectedState,
  state => state.selectedAccount?.wallets || [],
);

export const makeSelectKDAWallet = createSelector(selectedState, state =>
  state.selectedAccount?.wallets?.find(item => item.tokenAddress === 'coin'),
);

export const makeSelectAccountBalanceUsd = createSelector(
  selectedState,
  state => {
    const totalBalanceForAccount = (
      state.selectedAccount?.wallets || []
    ).reduce((accumAcc: number, walletItem: TWallet) => {
      const usdEquivalent = Array.isArray(state.usdEquivalents)
        ? state.usdEquivalents?.find(
            item => item.token === walletItem.tokenAddress,
          )?.usd || 0
        : 0;
      accumAcc += walletItem.totalAmount * usdEquivalent;
      return accumAcc;
    }, 0);
    return totalBalanceForAccount || 0;
  },
);

export const makeSelectTotalBalanceUsd = createSelector(
  selectedState,
  state => {
    const totalBalanceForAccounts = (state.accounts || []).reduce(
      (accum: number, account: TAccount) => {
        const totalBalanceForAccountItem = (account.wallets || []).reduce(
          (accumAcc: number, walletItem: TWallet) => {
            const usdEquivalent = Array.isArray(state.usdEquivalents)
              ? state.usdEquivalents?.find(
                  item => item.token === walletItem.tokenAddress,
                )?.usd || 0
              : 0;
            accumAcc += walletItem.totalAmount * usdEquivalent;
            return accumAcc;
          },
          0,
        );
        accum += totalBalanceForAccountItem;
        return accum;
      },
      0,
    );
    return totalBalanceForAccounts || 0;
  },
);

export const makeSelectUsdEquivalents = createSelector(selectedState, state => {
  return state.usdEquivalents || [];
});

export const makeSelectGasUsdEquivalent = createSelector(
  selectedState,
  state => {
    if (Array.isArray(state.usdEquivalents)) {
      return state.usdEquivalents?.find(item => item.token === 'gas')?.usd || 0;
    }
    return 0;
  },
);

export const makeSelectWalletList = createSelector(
  selectedState,
  state => state.selectedAccount?.wallets || [],
);
