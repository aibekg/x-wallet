import {configureStore, combineReducers} from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from 'redux-persist';
import {MMKV} from 'react-native-mmkv';
import rootSaga from './saga';
import auth from './auth';
import userWallet from './userWallet';
import contacts from './contacts';
import networks from './networks';
import transfers from './transfer';
import history from './history';
import {Storage} from 'redux-persist/es/types';

const storage = new MMKV();
const MMKVStorage: Storage = {
  setItem: (key, value) => {
    storage.set(key, value);
    return Promise.resolve(true);
  },
  getItem: key => {
    const value = storage.getString(key);
    return Promise.resolve(value);
  },
  removeItem: key => {
    storage.delete(key);
    return Promise.resolve();
  },
};

const sagaMiddleware = createSagaMiddleware();
const middleware = [sagaMiddleware];

const userWalletPersistConfig = {
  key: 'userWallet',
  storage: MMKVStorage,
  blacklist: ['initialized'],
};

const contactsPersistConfig = {
  key: 'contacts',
  storage: MMKVStorage,
};

const historyPersistConfig = {
  key: 'history',
  storage: MMKVStorage,
};

const authPersistConfig = {
  key: 'auth',
  storage: MMKVStorage,
  blacklist: ['isAuthorized', 'newPinCode'],
};

const networkPersistConfig = {
  key: 'networks',
  storage: MMKVStorage,
};

const transferPersistConfig = {
  key: 'transfers',
  storage: MMKVStorage,
  blacklist: ['showTransferBubble'],
};

const rootReducer = combineReducers({
  auth: persistReducer(authPersistConfig, auth),
  userWallet: persistReducer(userWalletPersistConfig, userWallet),
  contacts: persistReducer(contactsPersistConfig, contacts),
  history: persistReducer(historyPersistConfig, history),
  networks: persistReducer(networkPersistConfig, networks),
  transfers: persistReducer(transferPersistConfig, transfers),
});

export const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }).concat(middleware),
});

sagaMiddleware.run(rootSaga);
export const persistor = persistStore(store);

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
