import {getFontFamily} from '../utils/typegraphy';

export const BOLD_MONTSERRAT = getFontFamily('Montserrat-Bold');
export const REGULAR_MONTSERRAT = getFontFamily('Montserrat-Regular');
export const SEMI_BOLD_MONTSERRAT = getFontFamily('Montserrat-SemiBold');
export const MEDIUM_MONTSERRAT = getFontFamily('Montserrat-Medium');

export const MAIN_COLOR = '#20264E';
