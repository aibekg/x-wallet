import {Platform} from 'react-native';

export const isIos = Platform.OS === 'ios';

export const GAS_PRICE = 0.000001;
export const GAS_LIMIT = 2500;

export const ECONOMY_GAS_PRICE = 0.00000001;
export const FAST_GAS_PRICE = 0.0001;

export const REQUIRED_MESSAGE = 'required';
